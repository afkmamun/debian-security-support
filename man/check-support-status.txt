
CHECK-SUPPORT-STATUS(1)
=======================

NAME
----
check-support-status - check installed packages for security support (debian-security-support)

VERSION
-------
Version [% VERSION %]

SYNOPSIS
--------
Search for packages whose support is limited, has already ended or
will end earlier than the distribution's end of life:

----
    check-support-status
----

Search for packages with ended support from a custom list, reporting
each package only once:

----
    check-support-status \
        --type ended \
        --status-db /path/to/status-db \
        --list /path/to/security-support-ended
----

OPTIONS
-------
*--list* 'FILE'::

Use the given file as the database of packages whose support ends at a
particular date or that is limited by specific conditions. The file
format is plain text in columns, separated by one or more whitespace
characters.
+
For `--type earlyend`:
+
--
* source package name
* last package version that will be supported
* the date support will end
* the rest (optional): details, and/or a URL for further information.
--
+
For `--type ended`:
+
--
* source package name
* last package version that is supported
* the date support was ended
* the rest (optional): details, and/or a URL for further information.
--
+
For `--type limited`:
+
--
* source package name
* the rest (optional): details, and/or a URL for further information.
--
+
If no "--list" is provided, the script is run for limited and
date-defined end of support, using the lists shipped in the package.
+
By default, check-support-status evaluates the status of the packages
according to the Debian version where it runs upon. This behavior can
be modified using the DEBIAN_VERSION environment variable, e.g.

    DEBIAN_VERSION=9 check-support-status

*--no-heading*::

Skips printing a headline.

*--status-db* 'FILE'::

Use the given file to record alerts so each affected package is
reported only once.
+
Default: No records, any affected package will be reported every time.

*--except* 'PACKAGES'::

Do not alert for the given binary packages (comma-separated list).
+
Default: Alert for all packages (no exceptions).

*--type* 'TYPE'::

One of the following:

   * "earlyend": Alert for packages whose support will end earlier
                 than the distribution's.
   * "ended": Alert for packages where security support has ended.
   * "limited": Alert for packages where security support is limited.

*--version, --Version, -V*::

Show the version number and exit.

BUGS
----
Installations with mixed distributions like half-stable, half-testing
are not supported.

AUTHOR
------
Christoph Biedl <debian.axhn@manchmal.in-ulm.de>

COPYRIGHT & LICENSE
-------------------
----
Copyright (C) 2014 Christoph Biedl <debian.axhn@manchmal.in-ulm.de>

This package is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This package is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>

On Debian systems, the complete text of the GNU General Public
License version 2 can be found in
"/usr/share/common-licenses/GPL-2".
----
