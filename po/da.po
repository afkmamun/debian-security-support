# Danish translation debian-security-support.
# This file is distributed under the same license as the debian-security-support package.
# Copyright (C) 2016 debian-security-support & nedenstående oversættere.
# Joe Hansen <joedalton2@yahoo.dk>, 2014, 2016.
# 
msgid ""
msgstr ""
"Project-Id-Version: debian-security-support\n"
"Report-Msgid-Bugs-To: debian-security-support@packages.debian.org\n"
"POT-Creation-Date: 2016-06-07 12:13+0200\n"
"PO-Revision-Date: 2016-05-16 17:34+0000\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../check-support-status.in:24
#, sh-format
msgid ""
"Unknown DEBIAN_VERSION $DEBIAN_VERSION. Valid values from "
"$DEB_LOWEST_VER_ID and $DEB_NEXT_VER_ID"
msgstr ""
"Ukendt DEBIAN_VERSION $DEBIAN_VERSION. Gyldige værdier fra "
"$DEB_LOWEST_VER_ID og $DEB_NEXT_VER_ID"

#: ../check-support-status.in:63
msgid "Failed to parse the command line parameters"
msgstr "Kunne ikke fortolke kommandolinjeparametrene"

#: ../check-support-status.in:72
#, sh-format
msgid "$name version $VERSION"
msgstr "$name version $VERSION"

#: ../check-support-status.in:101
msgid "E: Internal error"
msgstr "E: Intern fejl"

#: ../check-support-status.in:117
msgid "E: Need a --type if --list is given"
msgstr "E: Skal bruge en --type hvis --list er angivet"

#: ../check-support-status.in:130
#, sh-format
msgid "E: Unknown --type '$TYPE'"
msgstr "E: Ukendt --type »$TYPE«"

#: ../check-support-status.in:282
msgid "Future end of support for one or more packages"
msgstr "Fremtidig stop for understøttelse for en eller flere pakker"

#: ../check-support-status.in:285
msgid ""
"Unfortunately, it will be necessary to end security support for some "
"packages before the end of the regular security maintenance life "
"cycle."
msgstr ""
"Desværre har det været nødvendigt at afslutte "
"sikkerhedsunderstøttelse for nogle pakker før slutningen på den "
"normale livscyklus for sikkerhedsvedligeholdelse."

#: ../check-support-status.in:288 ../check-support-status.in:298
#: ../check-support-status.in:308
msgid ""
"The following packages found on this system are affected by this:"
msgstr "De følgende pakker fundet på dit system er påvirket af dette:"

#: ../check-support-status.in:292
msgid "Ended security support for one or more packages"
msgstr "Afsluttet sikkerhedsunderstøttelse for en eller flere pakker"

#: ../check-support-status.in:295
msgid ""
"Unfortunately, it has been necessary to end security support for some "
"packages before the end of the regular security maintenance life "
"cycle."
msgstr ""
"Desværre har det været nødvendigt at afslutte "
"sikkerhedsunderstøttelse for nogle pakker før slutningen på den "
"normale livscyklus for sikkerhedsvedligeholdelse."

#: ../check-support-status.in:302
msgid "Limited security support for one or more packages"
msgstr "Begrænset sikkerhedsunderstøttelse for en eller flere pakker"

#: ../check-support-status.in:305
msgid ""
"Unfortunately, it has been necessary to limit security support for "
"some packages."
msgstr ""
"Desværre har det været nødvendigt at begrænse "
"sikkerhedsunderstøttelse for nogle pakker."

#: ../check-support-status.in:320
#, sh-format
msgid "* Source:$SRC_NAME, will end on $ALERT_WHEN"
msgstr "* Source:$SRC_NAME, vil slutte den $ALERT_WHEN"

#: ../check-support-status.in:323
#, sh-format
msgid ""
"* Source:$SRC_NAME, ended on $ALERT_WHEN at version $ALERT_VERSION"
msgstr ""
"* Source:$SRC_NAME, sluttede den $ALERT_WHEN med version "
"$ALERT_VERSION"

#: ../check-support-status.in:326
#, sh-format
msgid "* Source:$SRC_NAME"
msgstr "* Source:$SRC_NAME"

#: ../check-support-status.in:330
#, sh-format
msgid "  Details: $ALERT_WHY"
msgstr "  Detaljer: $ALERT_WHY"

#: ../check-support-status.in:333
msgid "  Affected binary package:"
msgstr "  Påvirket binær pakke:"

#: ../check-support-status.in:335
msgid "  Affected binary packages:"
msgstr "  Påvirkede binære pakker:"

#: ../check-support-status.in:338
#, sh-format
msgid "  - $BIN_NAME (installed version: $BIN_VERSION)"
msgstr "  - $BIN_NAME (installerede version: $BIN_VERSION)"
